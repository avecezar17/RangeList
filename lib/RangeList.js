// Task: Implement a class named 'RangeList'
// A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
// A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)

/**
 *
 * NOTE: Feel free to add any extra member variables/functions you like.
 */

const logger = require('../logger/logger');
const utils = require('../utils/utils');

let instance = null;

class RangeList {
  constructor() {
    if (!instance) {
      instance = this;
    }

    // range array
    this.range = [];
    // range view string for "print" method
    this.rangeView = '';
  }

  /**
   * Check incoming data (correct range format)
   */
  incomingDataCheck(range) {
    if (!range) {
      utils.handleError('No incoming data.');
    }
    if (!Array.isArray(range)) {
      utils.handleError('Wrong format of incoming data.');
    }
    if (range.length !== 2) {
      utils.handleError('Wrong range length.');
    }
    if (!Number.isInteger(range[0]) || !Number.isInteger(range[1])) {
      utils.handleError('Wrong range numbers format.');
    }
    if (range[0] > range[1]) {
      utils.handleError('Wrong sequence of numbers in range.');
    }
  }

  /**
   * Change rangeView value for "print" method
   */
  setRangeView() {
    if (this.range.length <= 1) {
      this.rangeView = '[0, 0)';
    } else {
      this.rangeView = '';
      for (let i = this.range[0]; i <= this.range[this.range.length - 1]; i += 1) {
        if (this.range.includes(i) && !this.range.includes(i - 1)) {
          this.rangeView += `[${i}, `;
        }
        if (this.range.includes(i) && !this.range.includes(i + 1)) {
          this.rangeView += `${i + 1}) `;
        }
      }
      this.rangeView = this.rangeView.trim();
    }
  }

  /**
   * Adds a range to the list
   * @param {Array<number>} range - Array of two integers that specify beginning and end of range.
   */
  add(range) {
    try {
      this.incomingDataCheck(range);
      const mergedRange = [...this.range, ...utils.rangeToArray(range[0], range[1])];
      this.range = mergedRange.filter((item, index) => mergedRange.indexOf(item) === index).sort((a, b) => a - b);
      this.setRangeView();
    } catch (e) {
      process.exit(1);
    }
  }

  /**
   * Removes a range from the list
   * @param {Array<number>} range - Array of two integers that specify beginning and end of range.
   */
  remove(range) {
    try {
      this.incomingDataCheck(range);
      utils.rangeToArray(range[0], range[1]).forEach((item) => {
        const index = this.range.indexOf(item);
        if (index > -1) {
          this.range.splice(index, 1);
        }
      });
      this.setRangeView();
    } catch (e) {
      process.exit(1);
    }
  }

  /**
   * Prints out the list of ranges in the range list
   */
  print() {
    logger.info(this.rangeView);
  }
}

module.exports = RangeList;
