const logger = require('../logger/logger');

const rangeToArray = (start, end) => {
  const array = [];
  for (let i = start; i < end; i += 1) {
    array.push(i);
  }
  return array;
};

const handleError = message => {
  logger.error(message);
  throw new Error(message);
};

module.exports = {
  rangeToArray,
  handleError,
};
