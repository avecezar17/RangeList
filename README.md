## A class named 'RangeList'.
- A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
- A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)

## Getting Started
To install all node_modules run the following command
```
$ npm i
```
### Structure
- lib - main class directory
- logger - winston logger directory for showing results and errors
- tests - mocha tests directory
- utils - directory for universal functions
- run.js - script with examples

## Commands
```
1. npm run run
2. npm run tests
3. npm run eslint
```
1. starts "run.js"
2. starts mocha tests in "tests" directory
3. run eslint --fix through all working files

