const RangeList = require('./lib/RangeList');

// Example run
const rangeList = new RangeList();

rangeList.add([1, 5]);
rangeList.print();
// Should display: [1, 5)

rangeList.add([10, 20]);
rangeList.print();
// Should display: [1, 5) [10, 20)

rangeList.add([20, 20]);
rangeList.print();
// Should display: [1, 5) [10, 20)

rangeList.add([20, 21]);
rangeList.print();
// Should display: [1, 5) [10, 21)

rangeList.add([2, 4]);
rangeList.print();
// Should display: [1, 5) [10, 21)

rangeList.add([3, 8]);
rangeList.print();
// Should display: [1, 8) [10, 21)

rangeList.remove([10, 10]);
rangeList.print();
// Should display: [1, 8) [10, 21)

rangeList.remove([10, 11]);
rangeList.print();
// Should display: [1, 8) [11, 21)

rangeList.remove([15, 17]);
rangeList.print();
// Should display: [1, 8) [11, 15) [17, 21)

rangeList.remove([3, 19]);
rangeList.print();
// Should display: [1, 3) [19, 21)
