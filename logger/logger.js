const { createLogger, format, transports } = require('winston');

const { combine, colorize, printf } = format;

const logger = createLogger({
  transports: [
    new transports.Console({
      format: combine(colorize(), printf(info => `${info.level.includes('error') ? 'ERROR: ' : ''}${info.message}`)),
    }),
  ],
});

module.exports = logger;
