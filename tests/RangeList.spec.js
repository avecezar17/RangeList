require('mocha');
const chai = require('chai');
const sinon = require('sinon');
const logger = require('../logger/logger');
const RangeList = require('../lib/RangeList');

const sandbox = sinon.createSandbox();

describe('Test /lib/RangeList', function () {
  describe('Test "incomingDataCheck" method', function () {
    it('should throw with "No incoming data." error', function (done) {
      const rangeList = new RangeList();
      const range = undefined;
      const errorMessage = 'No incoming data.';
      try {
        rangeList.incomingDataCheck(range);
      } catch (e) {
        chai.assert.deepEqual(e.message, errorMessage);
        done();
      }
    });
    it('should throw with "Wrong format of incoming data." error', function (done) {
      const rangeList = new RangeList();
      const range = {};
      const errorMessage = 'Wrong format of incoming data.';
      try {
        rangeList.incomingDataCheck(range);
      } catch (e) {
        chai.assert.deepEqual(e.message, errorMessage);
        done();
      }
    });
    it('should throw with "Wrong range length." error', function (done) {
      const rangeList = new RangeList();
      const range = [1, 2, 3];
      const errorMessage = 'Wrong range length.';
      try {
        rangeList.incomingDataCheck(range);
      } catch (e) {
        chai.assert.deepEqual(e.message, errorMessage);
        done();
      }
    });
    it('should throw with "Wrong range numbers format." error', function (done) {
      const rangeList = new RangeList();
      const range = [1.1, 3];
      const errorMessage = 'Wrong range numbers format.';
      try {
        rangeList.incomingDataCheck(range);
      } catch (e) {
        chai.assert.deepEqual(e.message, errorMessage);
        done();
      }
    });
    it('should throw with "Wrong sequence of numbers in range." error', function (done) {
      const rangeList = new RangeList();
      const range = [3, 1];
      const errorMessage = 'Wrong sequence of numbers in range.';
      try {
        rangeList.incomingDataCheck(range);
      } catch (e) {
        chai.assert.deepEqual(e.message, errorMessage);
        done();
      }
    });
  });

  describe('Test "setRangeView" method', function () {
    it('should set default value for rangeView', function (done) {
      const rangeList = new RangeList();
      const range = [];
      const rangeView = '[0, 0)';
      try {
        rangeList.setRangeView();
      } catch (e) {
        done(e);
      }
      chai.assert.deepEqual(rangeList.range, range);
      chai.assert.deepEqual(rangeList.rangeView, rangeView);
      done();
    });
    it('should set right value for rangeView', function (done) {
      const rangeList = new RangeList();
      const range = [1, 2, 3, 4];
      const rangeView = '[1, 5)';
      rangeList.range = [...range];
      try {
        rangeList.setRangeView();
      } catch (e) {
        done(e);
      }
      chai.assert.deepEqual(rangeList.range, range);
      chai.assert.deepEqual(rangeList.rangeView, rangeView);
      done();
    });
  });

  describe('Test "add" method', function () {
    it('should set right range and rangeView values', function (done) {
      const rangeList = new RangeList();
      const range = [10, 20];
      rangeList.range = [1, 2, 3, 4];
      rangeList.rangeView = '[1, 5)';
      const expectedRange = [1, 2, 3, 4, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19];
      const expectedRangeView = '[1, 5) [10, 20)';

      const mockRangeList = sandbox.mock(rangeList);
      mockRangeList
        .expects('incomingDataCheck')
        .withExactArgs(range);
      mockRangeList
        .expects('setRangeView')
        .withExactArgs()
        .callsFake(() => {
          rangeList.rangeView = expectedRangeView;
        });

      try {
        rangeList.add(range);
      } catch (e) {
        sandbox.restore();
        done(e);
      }
      chai.assert.deepEqual(rangeList.range, expectedRange);
      chai.assert.deepEqual(rangeList.rangeView, expectedRangeView);
      sandbox.restore();
      done();
    });
  });

  describe('Test "remove" method', function () {
    it('should set right range and rangeView values', function (done) {
      const rangeList = new RangeList();
      const range = [15, 17];
      rangeList.range = [1, 2, 3, 4, 5, 6, 7, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
      rangeList.rangeView = '[1, 8) [11, 21)';
      const expectedRange = [1, 2, 3, 4, 5, 6, 7, 11, 12, 13, 14, 17, 18, 19, 20];
      const expectedRangeView = '[1, 8) [11, 15) [17, 21)';

      const mockRangeList = sandbox.mock(rangeList);
      mockRangeList
        .expects('incomingDataCheck')
        .withExactArgs(range);
      mockRangeList
        .expects('setRangeView')
        .withExactArgs()
        .callsFake(() => {
          rangeList.rangeView = expectedRangeView;
        });

      try {
        rangeList.remove(range);
      } catch (e) {
        sandbox.restore();
        done(e);
      }
      chai.assert.deepEqual(rangeList.range, expectedRange);
      chai.assert.deepEqual(rangeList.rangeView, expectedRangeView);
      sandbox.restore();
      done();
    });
  });

  describe('Test "print" method', function () {
    it('should call logger', function (done) {
      const rangeList = new RangeList();
      const rangeView = '[1, 3) [19, 21)';
      rangeList.rangeView = rangeView;

      const mockRangeList = sandbox.mock(logger);
      mockRangeList
        .expects('info')
        .withExactArgs(rangeView);

      try {
        rangeList.print();
      } catch (e) {
        sandbox.restore();
        done(e);
      }
      sandbox.restore();
      done();
    });
  });
});
